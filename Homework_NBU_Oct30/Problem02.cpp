#include <iostream>
#include <array>

using namespace std;

int main()
{
	array<int, 3>  num;
	cout << "Enter three numbers:" << endl;
	cin >> num[0] >> num[1] >> num[2];

	bool duplicates = false;

	if (num[0] == num[1] || num[0] == num[2] || num[1] == num[2])
	{
		duplicates = true;
	}

	if (duplicates)
	{
		cout << "There are duplicate numbers" << endl;
	}
	else
	{
		cout << "There are no duplicate numbers" << endl;
	}

	return 0;
}