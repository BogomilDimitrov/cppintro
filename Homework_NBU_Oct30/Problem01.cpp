#include <iostream>
#include <string>

using namespace std;

int main()
{
	string firstName, lastName;

	cout << "Enter your name:" << endl;
	cin >> firstName >> lastName;

	cout << lastName << ", " << firstName << endl;

	return 0;
}