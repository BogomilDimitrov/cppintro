#include <iostream>

using namespace std;

int main()
{
	char operation;
	double firstOperand, secondOperand;
	double result;
	
	cout << "Enter operation(a/s/m/p)" << endl;
	cin >> operation;
	cout << "Enter operand 1:" << endl;
	cin >> firstOperand;
	cout << "Enter operand 2:" << endl;
	cin >> secondOperand;

	switch (operation)
	{
		case 'a':
			result = firstOperand + secondOperand;
			cout << firstOperand << " + " << secondOperand << " = " << result << endl;
			break;
		case 's':
			result = firstOperand - secondOperand;
			cout << firstOperand << " - " << secondOperand << " = " << result << endl;
			break;
		case 'm':
			result = firstOperand * secondOperand;
			cout << firstOperand << " * " << secondOperand << " = " << result << endl;
			break;
		case 'p':
			result = firstOperand / secondOperand;
			cout << firstOperand << " / " << secondOperand << " = " << result << endl;
			break;
	}

	

	return 0;
}